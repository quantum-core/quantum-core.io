---
layout: default
---
# What is QuantumCore?

QuantumCore is an open source Metin2 server emulator.
This project is currently still work in progress, visit the [Blog](/blog) for
recent status updates.

## Plannes features

- Full scripting support
- Compatible with standard so called 40k client
- Fast and efficient
- Modern technologies (.NET Core, Redis, Docker, ...)